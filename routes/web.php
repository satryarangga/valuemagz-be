<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('user/change-status/{id}/{status}', 'UserController@changeStatus')->name('user.change-status');
Route::resource('user', 'UserController');
Route::resource('/', 'UserController');
Route::resource('home', 'MagazineController');

Route::get('article/change-status/{id}/{status}', 'ArticleController@changeStatus')->name('article.change-status');
Route::resource('article', 'ArticleController');

Route::get('magazine/change-status/{id}/{status}', 'MagazineController@changeStatus')->name('magazine.change-status');
Route::get('magazine/change-status-buyable/{id}/{status}', 'MagazineController@changeStatusBuyable')->name('magazine.change-status-buyable');
Route::get('magazine/bundling/{id}', 'MagazineController@sendBundling')->name('magazine.send-bundling');
Route::resource('magazine', 'MagazineController');

Route::get('register-transaction', 'CustomerController@registerTransaction')->name('customer.register-transaction');
Route::post('register-transaction', 'CustomerController@processRegisterTransaction')->name('customer.process-register-transaction');
Route::get('customer/change-status/{id}/{status}', 'CustomerController@changeStatus')->name('customer.change-status');
Route::resource('customer', 'CustomerController');

Route::get('webinar/change-status/{id}/{status}', 'WebinarController@changeStatus')->name('webinar.change-status');
Route::resource('webinar', 'WebinarController');

Route::get('slider/change-status/{id}/{status}', 'SliderController@changeStatus')->name('slider.change-status');
Route::resource('slider', 'SliderController');

Route::get('podcast/change-status/{id}/{status}', 'PodcastController@changeStatus')->name('podcast.change-status');
Route::resource('podcast', 'PodcastController');

Route::get('youtube/change-status/{id}/{status}', 'YoutubeController@changeStatus')->name('youtube.change-status');
Route::resource('youtube', 'YoutubeController');

Route::resource('bundling', 'BundlingController');

Route::resource('static-content', 'StaticContentController');


Route::get('download/{transactionId}/{purchaseCode}/{type}', 'HomeController@download')->name('download');
Route::get('download-bundling/{transactionId}/{type}', 'HomeController@downloadSubscriber')->name('download.subscriber');

Route::get('transaction/change-status/{id}/{status}', 'TransactionController@changeStatus')->name('transaction.change-status');
Route::get('transaction/send-link/{id}', 'TransactionController@sendLink')->name('transaction.send-link');
Route::get('transaction/send-bundling/{id}', 'MagazineController@sendBundlingExisted')->name('transaction.send-bundling-existed');
Route::get('transaction/register-bundling/{customerId}/{magazineId}', 'MagazineController@sendBundlingRegister')->name('transaction.send-bundling-register');
Route::get('transaction/export', 'TransactionController@export')->name('transaction.export');
Route::resource('transaction', 'TransactionController');
