@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">View {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Email</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" value="{{$row->email}}" readonly >
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Name</label>
	                  <div class="col-sm-8">
	                  	<input readonly type="text" class="form-control" name="name" value="{{$row->name}}" id="name" placeholder="Last Name">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Phone Number</label>
	                  <div class="col-sm-8">
	                  	<input readonly type="text" class="form-control" name="name" value="{{$row->phone_number}}" id="name" placeholder="Last Name">
	                  </div>
	                </div>

									<div class="form-group" style="display:none">
	                  <label for="name" class="col-sm-4 control-label">Gender</label>
	                  <div class="col-sm-8">
	                  	<input readonly type="text" class="form-control" name="name" value="{{$row->gender}}" id="name" placeholder="Last Name">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Birthdate</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="birthdate" value="{{date('j F Y', strtotime($row->birthdate))}}" id="datepicker_birthdate" placeholder="Last Name">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Bundling Quota</label>
	                  <div class="col-sm-8">
	                  	<input readonly type="text" class="form-control" name="name" value="{{$row->bundling_quota}}" id="name" placeholder="Last Name">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Reset Password</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="password" id="name" placeholder="Set New Password">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>

						<div class="box">
            	<div class="box-header with-border">
	              <h3 class="box-title">Owned Magazine</h3>
	            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>Magazine</th>
                  <th>Owned</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($magazine as $key => $val)
                <tr>
                <td>{{$val->title}}</td>
                <td>{{($val->is_owned) ? 'Yes' : 'No'}}</td>
                <td>
                	<div class="btn-group">
	                  <button type="button" class="btn btn-info">Action</button>
	                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
	                    <span class="caret"></span>
	                    <span class="sr-only">Toggle Dropdown</span>
	                  </button>
	                  <ul class="dropdown-menu" role="menu">
											@if(isset($val->transaction_id))
                      <li><a href="{{ route('transaction.send-link', ['id' => $val->transaction_id]) }}?customer=1">Send to Email</a></li>
											@elseif(isset($val->transaction_bundling_id))
											<li><a href="{{ route('transaction.send-bundling-existed', ['id' => $val->transaction_bundling_id]) }}">Send to Email</a></li>
											@else
											<li><a href="{{ route('transaction.send-bundling-register', ['customerId' => $row->id, 'magazineId' => $val->id]) }}">Send to Email</a></li>
											@endif
	                  </ul>
                	</div>
                </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
    </section>

@endsection