@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Edit {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                
	                <div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Title</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="title" value="{{$row->title}}" id="title" placeholder="Title">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Description</label>
	                  <div class="col-sm-8">
	                    <textarea name="description" class="form-control" rows="20">{{$row->description}}</textarea>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Embed Code</label>
	                  <div class="col-sm-8">
	                    <textarea name="embed_code" class="form-control" rows="20">{{$row->embed_code}}</textarea>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">Image Banner</label>
	                  <div class="col-sm-8">
	                  	<img style="width: 300px;height: 150px;margin-bottom: 20px" src="{{asset('images') . '/podcast/'.$row->banner}}" class="img-responsive">
	                    <input type="file" name="banner">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection