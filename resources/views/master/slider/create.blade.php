@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Create New {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
	            <form class="form-horizontal" action="{{route("$page.store")}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">

	                <div class="form-group hide">
	                  <label for="link" class="col-sm-3 control-label">Link</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="link" value="{{old('link')}}" id="link" placeholder="Link">
	                  </div>
	                </div>

	                <div class="form-group hide">
	                  <label for="target" class="col-sm-3 control-label">Target</label>
	                  <div class="col-sm-8">
	                  	<label class="radio-inline">
					      <input @if(old('target') == 1) checked @endif type="radio" name="target" value="1">Open Same Tab
					    </label>
					    <label class="radio-inline">
					      <input @if(old('target') == 2) checked @endif type="radio" name="target" value="2">Open New Tab
					    </label>
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">Image Slider</label>
	                  <div class="col-sm-8">
	                    <input type="file" name="filename">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	            </form>
	          </div>
          </div>
    </section>

@endsection