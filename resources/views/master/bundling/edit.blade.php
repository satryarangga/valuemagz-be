@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">View {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Label</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="label" value="{{$row->label}}">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Price</label>
	                  <div class="col-sm-8">
	                  	<input type="number" class="form-control" name="price" value="{{$row->price}}" />
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-4 control-label">Quota</label>
	                  <div class="col-sm-8">
	                  	<input type="number" class="form-control" name="quota" value="{{$row->quota}}" />
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection