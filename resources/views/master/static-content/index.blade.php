@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
	<div class="col-md-12">
		{!! session('displayMessage') !!}
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>Code</th>
                  <th>Content</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $key => $val)
                <tr>
                <td>{{$val->code}}</td>
                @if($val->type == 1)
                <td>{{(strlen($val->param) > 40) ? substr(strip_tags($val->param), 0, 40)."..." : $val->param}}</td>
                @elseif($val->type == 2)
                <td>{{(strlen($val->param) > 40) ? substr($val->param, 0, 40)."..." : $val->param}}</td>
                @else
                <td>
                  <img src="{{asset('images/contents')}}/{{$val->param}}" class="img-responsive" />
                </td>
                @endif
                <td>
                	<div class="btn-group">
	                  <button type="button" class="btn btn-info">Action</button>
	                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
	                    <span class="caret"></span>
	                    <span class="sr-only">Toggle Dropdown</span>
	                  </button>
	                  <ul class="dropdown-menu" role="menu">
                      <li><a href="{{ route($page.'.edit', ['id' => $val->id]) }}">Edit</a></li>
	                  </ul>
                	</div>
                </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
	</div>
</section>

@endsection