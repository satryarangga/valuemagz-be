@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Edit {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                
	                <div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Code</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="code" value="{{$row->code}}" id="code" readonly placeholder="Code">
	                  </div>
	                </div>

	                <div class="form-group hide">
	                  <label for="code" class="col-sm-3 control-label">Type</label>
	                  <div class="col-sm-8">
	                    <label class="radio-inline">
	                    	<input type="radio" name="type" value="1" @if($row->type == 1) checked @endif>Content
	                    </label>
										<label class="radio-inline">
											<input type="radio" name="type" value="2" @if($row->type == 2) checked @endif>Value
										</label>
										<label class="radio-inline">
										<input type="radio" name="type" value="3" @if($row->type == 3) checked @endif>Value
									</label>
	                  </div>
	                </div>

	                <div class="form-group" id="contentGroup" style="display:{{($row->type == 1) ? 'block' : 'none'}}">
	                  <label for="name" class="col-sm-3 control-label">Content</label>
	                  <div class="col-sm-8">
	                    <textarea name="content" class="form-control" rows="20">{{$row->param}}</textarea>
	                  </div>
	                </div>

									<div class="form-group" id="valueGroup" style="display:{{($row->type == 2) ? 'block' : 'none'}}">
	                  <label for="content" class="col-sm-3 control-label">Value</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="value" value="{{$row->param}}" id="title" placeholder="Value">
	                  </div>
	                </div>

									<div class="form-group" id="imgGroup" style="display:{{($row->type == 3) ? 'block' : 'none'}}">
	                  <label for="content" class="col-sm-3 control-label">Value</label>
	                  <div class="col-sm-8">
	                    <input type="file" class="form-control" name="image" name="value">
											<img src="{{asset('images/contents')}}/{{$row->param}}" class="img-responsive" />
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection