@extends('layouts.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
	            <form class="form-horizontal" action="{{route("$page.store")}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="code" class="col-sm-3 control-label">Code</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="code" value="{{old('code')}}" id="title" placeholder="Code">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="code" class="col-sm-3 control-label">Type</label>
	                  <div class="col-sm-8">
	                    <label class="radio-inline"><input type="radio" name="type" value="1" checked>Content</label>
											<label class="radio-inline"><input type="radio" name="type" value="2">Value</label>
	                  </div>
	                </div>

	                <div class="form-group" id="contentGroup">
	                  <label for="content" class="col-sm-3 control-label">Content</label>
	                  <div class="col-sm-8">
	                    <textarea name="content" class="form-control" rows="20">{{old('content')}}</textarea>
	                  </div>
	                </div>

	                <div class="form-group" style="display: none;" id="valueGroup">
	                  <label for="content" class="col-sm-3 control-label">Value</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="value" value="{{old('value')}}" id="title" placeholder="Value">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	            </form>
	          </div>
          </div>
    </section>

@endsection