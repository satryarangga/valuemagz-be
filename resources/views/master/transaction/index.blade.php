@extends('layout.main')

@section('title', 'Home')

@section('content')

<section class="content">
	<div class="col-md-12">
		{!! session('displayMessage') !!}
		<div class="box">
            <div class="box-header">
                <form>
                  <input type="text" id="datepicker" name="start_date" placeholder="Start Date" />
                  <input type="text" id="datepicker2" name="end_date" placeholder="End Date" />
                  <input class="btn btn-success" type="submit" value="Filter" />
                </form>
                <a style="float:left" class="btn btn-primary" href="{{route('transaction.export')}}?start_date={{$startDate}}&end_date={{$endDate}}">Export Excel</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                  <th>Purchase Code</th>
                  <th>Customer</th>
                  <th>Magazine</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $key => $val)
                <tr>
                <td>{{$val->purchase_code}}</td>
                <td>{{$val->email}} - {{$val->phone_number}}</td>
                <td>{{$val->title}} @if($val->bundling_id == 1) (Bundling) @endif</td>
                @if(empty($val->bundling_id))
                <td>{{moneyFormat($val->price)}}</td>
                @else
                <td>{{moneyFormat($val->bundling_price)}}</td>
                @endif
                <td>{!! setPaymentStatus($val->payment_status) !!}</td>
                <td>
                	<div class="btn-group">
	                  <button type="button" class="btn btn-info">Action</button>
	                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
	                    <span class="caret"></span>
	                    <span class="sr-only">Toggle Dropdown</span>
	                  </button>
	                  <ul class="dropdown-menu" role="menu">
                      @if($val->payment_status == 0 || $val->payment_status == 3)
                      <li><a href="{{ route($page.'.change-status', ['id' => $val->id, 'status' => 1]) }}">Set Paid</a></li>
                      @endif
                      @if($val->payment_status == 1)
                      <li><a href="{{ route($page.'.send-link', ['id' => $val->id]) }}">Send PDF Link</a></li>
                      @endif
	                  </ul>
                	</div>
                </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
	</div>
</section>

@endsection