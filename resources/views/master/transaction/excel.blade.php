<table>
    <thead>
    <tr>
        <th>Code</th>
        <th>Date</th>
        <th>Customer Email</th>
        <th>Customer Phone</th>
        <th>Magazine</th>
        <th>Type</th>
        <th>Price</th>
        <th>Status</th>
        <th>Payment Source</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($order as $key => $val)
        <tr>
            <td>{{ $val->purchase_code }}</td>
            <td>{{ date('j F Y', strtotime($val->created_at)) }}</td>
            <td>{{ $val->email }}</td>
            <td>{{ $val->phone_number }}</td>
            <td>{{ $val->magazine_name }}</td>
            <td>{{ (empty($val->bundling_id)) ? 'Single' : 'Bundling' }}</td>
            <td>{{ (empty($val->bundling_id)) ? $val->price : $val->bundling_price }}</td>
            <td>{{ setPaymentStatusRaw($val->payment_status) }}</td>
            <td>{{ $val->payment_source }}</td>
        </tr>
    @endforeach
    </tbody>
</table>