@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Create New {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
	            <form class="form-horizontal" action="{{route("$page.store")}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="title" class="col-sm-3 control-label">Title</label>
	                  <div class="col-sm-8">
	                    <input type="text" class="form-control" name="title" value="{{old('title')}}" id="title" placeholder="Title">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="synopsis" class="col-sm-3 control-label">Synopsis</label>
	                  <div class="col-sm-8">
	                    <textarea name="synopsis" class="form-control" rows="20">{{old('synopsis')}}</textarea>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="price" class="col-sm-3 control-label">Price</label>
	                  <div class="col-sm-8">
											<input type="number" class="form-control" name="price" value="{{old('price')}}" id="price" placeholder="price">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">Banner</label>
	                  <div class="col-sm-8">
	                    <input type="file" name="banner">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">PDF File</label>
	                  <div class="col-sm-8">
	                    <input type="file" accept="application/pdf" name="magazine">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">PDF File Mobile</label>
	                  <div class="col-sm-8">
	                    <input type="file" accept="application/pdf" name="magazine_mobile">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">PDF File English</label>
	                  <div class="col-sm-8">
	                    <input type="file" accept="application/pdf" name="magazine_en">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">PDF File English Mobile</label>
	                  <div class="col-sm-8">
	                    <input type="file" accept="application/pdf" name="magazine_en_mobile">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	            </form>
	          </div>
          </div>
    </section>

@endsection