@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- Main content -->
    <section class="content">
    	<div class="col-md-12">
			<div class="box box-info">
	            <div class="box-header with-border">
	              <h3 class="box-title">Edit {{ucwords(str_replace('-',' ', $page))}}</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            @foreach($errors->all() as $message)
		            <div style="margin: 20px 0" class="alert alert-error">
		                {{$message}}
		            </div>
		        @endforeach
		        {!! session('displayMessage') !!}
	            <form class="form-horizontal" action="{{route("$page.update", ['id' => $row->id])}}" method="post" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              <div class="box-body">
	                
	                <div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Title</label>
	                  <div class="col-sm-8">
	                  	<input type="text" class="form-control" name="title" value="{{$row->title}}" id="title" placeholder="Title">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Synopsis</label>
	                  <div class="col-sm-8">
	                    <textarea name="synopsis" class="form-control" rows="20">{{$row->synopsis}}</textarea>
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="name" class="col-sm-3 control-label">Price</label>
	                  <div class="col-sm-8">
	                  	<input type="number" class="form-control" name="price" value="{{$row->price}}" id="price" placeholder="price">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">Banner</label>
	                  <div class="col-sm-8">
	                  	<img style="width: 100px;height: 150px;margin-bottom: 20px" src="{{asset('images') . '/magazines/'.$row->banner}}" class="img-responsive">
	                    <input type="file" name="banner">
	                  </div>
	                </div>

	                <div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">File PDF</label>
	                  <div class="col-sm-8">
	                  	<label>{{$row->filename}}</label>
											<input type="file" accept="application/pdf" name="magazine">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">File PDF Mobile</label>
	                  <div class="col-sm-8">
	                  	<label>{{$row->filename_mobile}}</label>
											<input type="file" accept="application/pdf" name="magazine_mobile">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">PDF File English</label>
	                  <div class="col-sm-8">
											<label>{{$row->filename_en}}</label>
											<input type="file" accept="application/pdf" name="magazine_en">
	                  </div>
	                </div>

									<div class="form-group">
	                  <label for="poster" class="col-sm-3 control-label">PDF File English Mobile</label>
	                  <div class="col-sm-8">
											<label>{{$row->filename_en_mobile}}</label>
											<input type="file" accept="application/pdf" name="magazine_en_mobile">
	                  </div>
	                </div>

	              </div>
	              <!-- /.box-body -->
	              <div class="box-footer">
	                <button type="submit" class="btn btn-info pull-right">Submit</button>
	              </div>
	              <!-- /.box-footer -->
	              {{ method_field('PUT') }}
	            </form>
	          </div>
          </div>
    </section>

@endsection