<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li @if($page == 'user') class="active" @endif>
          <a href="{{route('user.index')}}">
          <i class="fa fa-users"></i> <span>User</span>
          </a>
        </li>

        <li @if($page == 'bundling') class="active" @endif>
          <a href="{{route('bundling.index')}}">
          <i class="fa fa-money"></i> <span>Bundling</span>
          </a>
        </li>

        <li @if($page == 'article') class="active" @endif>
          <a href="{{route('article.index')}}">
          <i class="fa fa-image"></i> <span>Blog</span>
          </a>
        </li>

        <li @if($page == 'podcast') class="active" @endif>
          <a href="{{route('podcast.index')}}">
          <i class="fa fa-music"></i> <span>Podcast</span>
          </a>
        </li>

        <li @if($page == 'youtube') class="active" @endif>
          <a href="{{route('youtube.index')}}">
          <i class="fa fa-play"></i> <span>Youtube</span>
          </a>
        </li>

        <li @if($page == 'magazine') class="active" @endif>
          <a href="{{route('magazine.index')}}">
          <i class="fa fa-image"></i> <span>Magazine</span>
          </a>
        </li>

        <li @if($page == 'customer') class="active" @endif>
          <a href="{{route('customer.index')}}">
          <i class="fa fa-users"></i> <span>Customer</span>
          </a>
        </li>

        <li @if($page == 'transaction') class="active" @endif>
          <a href="{{route('transaction.index')}}">
          <i class="fa fa-money"></i> <span>Transaction</span>
          </a>
        </li>

        <li @if($page == 'customer') class="active" @endif>
          <a href="{{route('customer.register-transaction')}}">
          <i class="fa fa-money"></i> <span>Register User Transaction</span>
          </a>
        </li>

        <li @if($page == 'static-content') class="active" @endif>
          <a href="{{route('static-content.index')}}">
          <i class="fa fa-pencil"></i> <span>Static Content</span>
          </a>
        </li>
        
        <li @if($page == 'webinar') class="active" @endif>
          <a href="{{route('webinar.index')}}">
          <i class="fa fa-money"></i> <span>Webinar</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>