@if(isset($localAsset))
	@foreach($localAsset as $key => $val)
		@includeIf('util.js.'.$val)
	@endforeach
@endif

@includeIf($local_js_path.$page)