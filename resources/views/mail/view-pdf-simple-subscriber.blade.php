{!! $magazine->synopsis !!}

@if($customer->bundling_quota - 1 > 0)
<h2 style="font-size: 14px" class="aligncenter">Kamu masih berlangganan magazine value untuk <b>{{$customer->bundling_quota - 1}}</b> edisi selanjutnya</h2>
@else
<h2 style="font-size: 14px" class="aligncenter">Ini adalah edisi terakhir langganan kamu. Ayo langganan lagi untuk edisi selanjutnya di <a href="https://valuemagz.id">Valuemagz</a></h2>
@endif
<h2 style="font-size: 18px" class="aligncenter">Link to download print Bahasa version : {{route('download.subscriber', ['transactionId' => $transaction->id, 'type' => 'paper'])}}</h2>
<h2 style="font-size: 18px" class="aligncenter">Link to download print English version : {{route('download.subscriber', ['transactionId' => $transaction->id, 'type' => 'paper-en'])}}</h2>
<h2 style="font-size: 18px" class="aligncenter">Link to download digital Bahasa version : {{route('download.subscriber', ['transactionId' => $transaction->id, 'type' => 'digital'])}}</h2>
<h2 style="font-size: 18px" class="aligncenter">Link to download digital English version : {{route('download.subscriber', ['transactionId' => $transaction->id, 'type' => 'digital-en'])}}</h2>