Hai kamu,<br /><br />
Selamat ya kalian sudah mendapat email ini.<br />
Selanjutnya, silahkan melengkapi data kamu dengan melakukan verifikasi dengan data di bawah ini, karena account ini akan digunakan untuk menerima majalah-mu selanjutnya.<br /><br />
<a style="background-color:#ed1c09;padding:10px;color:#fff;margin-top:40px;text-decoration:none" href="{{config('value.frontend_site')}}/account/profile">Ubah data</a><br /><br />
<div style="border:1px solid #000;padding:10px;width:270px">
<h2 style="font-size: 16px">Informasi Login</h2>
Email : {{$customer->email}} <br />
Password: {{$customer->password}}
</div>
<p style="margin-top:20px">Ayo lengkapi datamu sekarang!</p>