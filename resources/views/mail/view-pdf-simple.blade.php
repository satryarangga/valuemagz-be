{!! $magazine->synopsis !!}

<h2 style="font-size: 18px" class="aligncenter">Link to download print Bahasa version : {{route('download', ['transactionId' => $transaction->id, 'purchaseCode' => $transaction->purchase_code, 'type' => 'paper'])}}</h2>
<h2 style="font-size: 18px" class="aligncenter">Link to download print English version : {{route('download', ['transactionId' => $transaction->id, 'purchaseCode' => $transaction->purchase_code, 'type' => 'paper-en'])}}</h2>
<h2 style="font-size: 18px" class="aligncenter">Link to download digital Bahasa version : {{route('download', ['transactionId' => $transaction->id, 'purchaseCode' => $transaction->purchase_code, 'type' => 'digital'])}}</h2>
<h2 style="font-size: 18px" class="aligncenter">Link to download digital English version : {{route('download', ['transactionId' => $transaction->id, 'purchaseCode' => $transaction->purchase_code, 'type' => 'digital-en'])}}</h2>