<?php

use Illuminate\Database\Seeder;

class BundlingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bundling_master')->truncate();
        DB::table('bundling_master')->insert([
         	'label' => 'Bundling 3 Edisi',
         	'price' => 1500000,
         	'quota' => 3
        ]);
    }
}
