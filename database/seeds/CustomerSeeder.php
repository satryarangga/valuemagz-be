<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('customers')->truncate();
        DB::table('customers')->insert([
         	'email' => 'admin@techbro.id',
         	'name' => 'Admin Techbro',
            'password' => bcrypt('123456'),
            'phone_number' => '082122702276',
            'gender' => 'M',
            'birthdate' => '1991-10-14'
        ]);
    }
}
