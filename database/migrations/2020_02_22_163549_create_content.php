<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->string('url', 100);
            $table->string('banner', 100)->nullable();
            $table->text('content')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0:not active, 1:active');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename', 255);
            $table->string('link')->nullable();
            $table->tinyInteger('target')->default(1)->comment('0:same tab, 1:new tab');
            $table->tinyInteger('status')->default(0)->comment('0:not active, 1:active');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
        Schema::dropIfExists('sliders');
    }
}
