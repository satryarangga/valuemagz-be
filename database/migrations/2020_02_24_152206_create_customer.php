<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('name', 255);
            $table->string('password', 255);
            $table->tinyInteger('status')->default(1)->comment('1:active, 0:not active');
            $table->string('phone_number', 100)->nullable();
            $table->string('gender', 5)->nullable();
            $table->date('birthdate')->nullable();
            $table->integer('province_id')->nullable();
            $table->text('address')->nullable();
            $table->string('province_name', 255)->nullable();
            $table->integer('city_id')->nullable();
            $table->string('city_name', 255)->nullable();
            $table->string('zipcode', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
