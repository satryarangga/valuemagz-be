<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMagazineBundling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bundling_transaction', function (Blueprint $table) {
            $table->integer('magazine_id')->after('customer_id');
            $table->tinyInteger('status')->default(0)->comment('0:not sent, 1:sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bundling_transaction', function (Blueprint $table) {
            $table->dropColumn('magazine_id');
            $table->dropColumn('status');
        });
    }
}
