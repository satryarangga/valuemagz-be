<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('bundling_quota')->default(0)->after('status');
        });

        Schema::create('bundling_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('executed_by');
            $table->timestamps();
        });

        Schema::create('bundling_master', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->integer('price');
            $table->integer('quota');
            $table->timestamps();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('grand_total')->default(0)->after('price');
            $table->integer('bundling_price')->default(0)->after('price');
            $table->integer('bundling_id')->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('bundling_quota');
        });

        Schema::dropIfExists('bundling_transaction');
        Schema::dropIfExists('bundling_master');

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('bundling_id');
            $table->dropColumn('bundling_price');
            $table->dropColumn('grand_total');
        });
    }
}
