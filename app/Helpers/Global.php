<?php

    /**
     * @param string $messageType
     * @param string $message
     * @return string
     */
    function setDisplayMessage($messageType = 'error', $message = 'Error Message')
    {
        $message = '<div style="margin:20px 0" class="alert alert-' . $messageType . '">' . $message . '</div>';
        return $message;
    }

    /**
     * @param $status
     * @param string $type
     * @return string
     */
    function setActivationStatus($status, $type = 'Active'){
        if($status == 0 || $status == 3){
            return '<span class="btn btn-default">Not '.$type.'</span>';
        } else if($status == 2) {
            return '<span class="btn btn-danger">Payment Failed</span>';
        }

        return '<span class="btn btn-success">'.$type.'</span>';
    }

    /**
     * @param $status
     * @param string $type
     * @return string
     */
    function setPaymentStatus($status){
        if($status == 0 || $status == 3){
            return '<span class="btn btn-danger">Unpaid</span>';
        } else if($status == 2) {
            return '<span class="btn btn-danger">Payment Failed</span>';
        } else if($status == 4) {
            return '<span class="btn btn-danger">Cancelled</span>';
        }

        return '<span class="btn btn-success">Paid</span>';
    }

    /**
     * @param $status
     * @param string $type
     * @return string
     */
    function setPaymentStatusRaw($status, $type = 'Active'){
        if($status == 0 || $status == 3 || $status == 4){
            return "Unpaid";
        } else if($status == 2) {
            return "Failed";
        }

        return "Paid";
    }

    function parseMoneyToInteger($money) {
        if($money == null){
            return 0;
        }
        $money = str_replace('.', '', $money); 
        return str_replace(',', '', $money);
    }

    /**
     * @param $money
     * @return string
     */
    function moneyFormat ($money, $useCurrency = true) {
        $currency = ($useCurrency) ? 'Rp. ' : '';
        $money = number_format($money,0,',',',');

        return $currency.$money;
    }

    function dateHumanFormat($date) {
        if($date == null) {
            return null;
        }

        return date('j F Y', strtotime($date));
    }

    // function logUser($desc) {
    //     return App\Models\UserLogs::createLog($desc);
    // }

    function getFieldOfTable($tableName, $primaryKey, $field) {
        $query = Illuminate\Support\Facades\DB::table($tableName)->where('id', $primaryKey)->value($field);

        return $query;
    }

    function urlFormat($name) {
        $constraint = [' ', '_'];
        return strtolower(str_replace($constraint, '-', $name));
    }

    function setFileExistLabel($exist) {
        if($exist) {
            return '<span class="label label-success">Yes</span>';
        }
        return '<span class="label label-danger">No</span>';
    }