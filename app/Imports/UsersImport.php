<?php

namespace App\Imports;

use App\Models\Customer;
use App\Models\Magazine;
use App\Models\User;
use App\Models\Transaction;
use App\Models\BundlingMaster;
use App\Models\EmailQueue;
use App\Models\BundlingTransaction;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendNotifLink;
use App\Mail\RegisterBundling as RegisterBundlingMail;

class UsersImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $register = false;
            // echo json_encode($row); die;
            $email = $row[1];
            if(empty($email)) {
                continue;   
            }
            $type = $row[2];
            $magazineId = config('value.active_magazine');
            $name = $row[0];
            $phoneNumber = $row[3];
            if(!isset($row[4]) || empty($row[4])) {
                $birthdate = null;
            } else {
                $birthdate = date('Y-m-d', ($birthdate-25569) * 86400);
            }
            $isBundling = $type == 'bundling';
            $customer = Customer::where('email', $email)->first();
            $objRegister = null;
            
            if(!isset($customer->id)) {
                $password = Str::random(10);
                $customer = Customer::create([
                    'email' => $email,
                    'name'  => $name,
                    'phone_number'  => $phoneNumber,
                    'birthdate'  => $birthdate,
                    'password' => bcrypt($password)
                ]);

                // SEND EMAIL REGISTER
                $data = [
                    'email' => $email,
                    'password' => $password,
                    'name' => $name
                ];
    
                $objRegister = (object) $data;
    
                // Mail::to($email)->send(new RegisterBundlingMail($obj, 'register'));
                $register = true;
            }

            $where = [
                ['customer_id', '=', $customer->id],
                ['magazine_id', '=', $magazineId],
                ['status', '=', 1]
            ];
            $whereTransaction = [
                ['customer_id', '=', $customer->id],
                ['magazine_id', '=', $magazineId],
                ['payment_status', '=', 1]
            ];
            $transaction = BundlingTransaction::where($where)->get();
            $transactionSingle = Transaction::where($whereTransaction)->count();
            if(count($transaction) == 0 && $transactionSingle == 0) {
                $purchaseCode = Str::random(8);
                $magazine = Magazine::find(intval($magazineId));
                $bundlingMaster = BundlingMaster::find(1);

                $transaction = Transaction::create([
                    'purchase_code' => strtoupper($purchaseCode),
                    'customer_id' => $customer->id,
                    'magazine_id' => $magazineId,
                    'price' => ($isBundling) ? 0 : $magazine->price,
                    'bundling_price' => ($isBundling) ? $bundlingMaster->price : 0,
                    'bundling_id' => ($isBundling) ? 1 : null,
                    'payment_status' => 1,
                    'payment_source' => 'gform'
                ]);

                if($isBundling) {
                    $buyer = Customer::find($transaction->customer_id);
                    $oldQuota = $buyer->bundling_quota;
                    $buyer->bundling_quota = $oldQuota + $bundlingMaster->quota - 1;
                    $buyer->save();
                }

                $objectEmail = [
                    'customer' => $customer,
                    'magazine'  => $magazine,
                    'transaction' => $transaction
                ];

                if($register) {
                    $objectEmail['register'] = $objRegister;
                }

                EmailQueue::create([
                    'email' => $email,
                    'type'  => 'excel-upload-transaction',
                    'json_object' => json_encode($objectEmail)
                ]);
                // SEND EMAIL MAGAZINE
                // Mail::to($customer['email'])->send(new SendNotifLink($customer, $magazine, $transaction));
            }
        }
    }
}
