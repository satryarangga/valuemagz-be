<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use App\Models\EmailQueue;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendNotifLink;
use App\Mail\SendNotifLinkRegister;

class SendAsyncEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email queued';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = EmailQueue::where('processed', 0)
                            ->limit(70)
                            ->get();

        foreach($list as $key => $val) {
            $email = $val->email;
            echo "Start Sending email ".$val->type." to $email\n";
            $object = json_decode($val->json_object);
            if($val->type == 'excel-upload-transaction') {
                if(isset($object->register)) {
                    Mail::to($email)->send(new SendNotifLinkRegister($object->customer, $object->magazine, $object->transaction, $object->register));
                } else {
                    Mail::to($email)->send(new SendNotifLink($object->customer, $object->magazine, $object->transaction));
                }
            }
            EmailQueue::find($val->id)->update([
                'processed' => 1
            ]);
            echo "Done Sending email ".$val->type." to $email\n";
        }
    }
}
