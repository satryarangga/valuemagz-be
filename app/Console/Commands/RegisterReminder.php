<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterBundling as RegisterBundlingMail;

class RegisterReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder for user who dont have birthdate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = Customer::where('name', 'Value User')
                            ->whereNull('phone_number')
                            ->whereNull('birthdate')
                            ->get();
        
        foreach($emails as $key => $val) {
            $customer = Customer::where('email', $val['email'])->first();
            $password = Str::random(10);

            $data = [
                'email' => $val['email'],
                'password' => $password,
                'name' => 'Value User'
            ];

            $obj = (object) $data;
            echo "Sending Reminder ".$customer['email']." email \n";
            Customer::find($customer->id)->update([
                'password' => bcrypt($password)
            ]);
            Mail::to($customer['email'])->send(new RegisterBundlingMail($obj, 'reminder'));

            echo "Success to send email reminder to ".$val['email']."\n";
        }
    }
}
