<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterBundling as RegisterBundlingMail;

class RegisterBundling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register bundling from list of email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = config('value.bundling_email');
        foreach($emails as $key => $val) {
            echo "Registering email ".$val['email']."\n";
            
            $customer = Customer::where('email', $val['email'])
                                    ->first();
            $password = Str::random(10);

            if(isset($customer['email'])) {
                echo "Email ".$customer['email']." is already registered \n";
                $data = [
                    'email' => $val['email'],
                    'password' => $password,
                    'name' => 'Value User'
                ];
    
                $obj = (object) $data;

                if(!$customer->phone_number && !$customer->birthdate && $customer->name == 'Value User') {
                    echo "Sending ".$customer['email']." email \n";
                    Customer::find($customer->id)->update([
                        'password' => bcrypt($password)
                    ]);
                    Mail::to($customer['email'])->send(new RegisterBundlingMail($obj, 'register'));
                }
                continue;
            }
            
            Customer::create([
                'email' => $val['email'],
                'name' => 'Value User',
                'password' => bcrypt($password),
                'status' => 1,
                'verified' => 1,
                'bundling_quota' => $val['quota']
            ]);

            $data = [
                'email' => $val['email'],
                'password' => $password,
                'name' => 'Value User'
            ];

            $obj = (object) $data;

            Mail::to($val['email'])->send(new RegisterBundlingMail($obj, 'register'));

            echo "Success to register email ".$val['email']."\n";
        }
    }
}
