<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Amqp;
use App\Models\BundlingTransaction;
use App\Models\Customer;
use App\Models\Magazine;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendNotifLinkSubscriber;

class ExecuteSubscriber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:subscriber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pdf email to eligible subscriber';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Amqp::consume('valuemagz.queue', function ($message, $resolver) {

            $transactionId = (int) $message->body;
            $bundlingTransaction = BundlingTransaction::find($transactionId);
            echo "Executing bundling transaction ".$transactionId."\n";

            if(isset($bundlingTransaction->id)) {
                $customer = Customer::find($bundlingTransaction->customer_id);
                $magazine = Magazine::find($bundlingTransaction->magazine_id);
                Mail::to($customer->email)->send(new SendNotifLinkSubscriber($customer, $magazine, $bundlingTransaction));
                $bundlingTransaction->status = 1;
                $bundlingTransaction->save();

                $oldQuota = $customer->bundling_quota;
                $customer->bundling_quota = $oldQuota - 1;
                $customer->save();
                $resolver->acknowledge($message);
                $resolver->stopWhenProcessed();
                echo "Success executing bundling transaction ".$transactionId."\n";
            } else {
                echo "Failed executing bundling transaction ".$transactionId."\n";
            }
                 
         });
    }
}
