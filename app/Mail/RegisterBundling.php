<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterBundling extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    public $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $type)
    {
        $this->customer = $customer;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'register') {
            return $this->subject('Selamat datang pembaca Value!')
                    ->view('mail.register');
        }

        return $this->subject('Ayo Lengkapi Datamu di Value!')
                    ->view('mail.reminder');
    }
}
