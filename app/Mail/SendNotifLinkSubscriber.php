<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotifLinkSubscriber extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    
    public $magazine;

    public $transaction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $magazine, $transaction)
    {
        $this->customer = $customer;
        $this->magazine = $magazine;
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('E-Magz Value! '.$this->magazine->title)
                    ->view('mail.view-pdf-subscriber');
    }
}
