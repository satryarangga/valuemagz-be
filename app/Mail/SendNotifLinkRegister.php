<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotifLinkRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    
    public $magazine;

    public $transaction;

    public $register;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $magazine, $transaction, $register)
    {
        $this->customer = $customer;
        $this->magazine = $magazine;
        $this->transaction = $transaction;
        $this->register = $register;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('E-Magz Value! '.$this->magazine->title)
                    ->view('mail.view-pdf-register');
    }
}
