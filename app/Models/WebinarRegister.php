<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebinarRegister extends Model
{
    /**
     * @var string
     */
    protected $table = 'webinar_register';

    /**
     * @var array
     */
    protected $fillable = [
        'webinar_id', 'customer_id'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';
}
