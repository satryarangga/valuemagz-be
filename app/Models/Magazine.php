<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Magazine extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = 'magazines';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'synopsis', 'filename', 'price', 'status', 'created_by', 'banner', 'filename_mobile', 'buyable',
        'filename_en', 'filename_en_mobile'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
