<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BundlingMaster extends Model
{
    /**
     * @var string
     */
    protected $table = 'bundling_master';

    /**
     * @var array
     */
    protected $fillable = [
        'label', 'price', 'quota'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';
}
