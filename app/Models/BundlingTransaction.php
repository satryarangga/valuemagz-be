<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BundlingTransaction extends Model
{
    /**
     * @var string
     */
    protected $table = 'bundling_transaction';

    /**
     * @var array
     */
    protected $fillable = [
        'customer_id', 'magazine_id', 'executed_by', 'status'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';
}
