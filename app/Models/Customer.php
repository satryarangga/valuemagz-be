<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * @var string
     */
    protected $table = 'customers';

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'name', 'password', 'status', 'phone_number', 'gender', 'birthdate', 'province_id', 'status',
        'address', 'province_name', 'city_id', 'city_name', 'zipcode', 'bundling_quota', 'verified'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
