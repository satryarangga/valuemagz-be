<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    /**
     * @var string
     */
    protected $table = 'transactions';

    /**
     * @var array
     */
    protected $fillable = [
        'purchase_code', 'customer_id', 'magazine_id', 'price', 'payment_status', 'bundling_id', 'bundling_price',
        'grand_total', 'payment_source'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    public static function list($startDate = null, $endDate = null) {
        $where = [];
        if(!empty($startDate)) {
            $where[] = ['transactions.created_at', '>', $startDate." 00:00:00"];
            $where[] = ['transactions.created_at', '<', $endDate." 23:59:59"];
        }
        $data = parent::select('transactions.*', 'magazines.title', 'customers.email', 'customers.phone_number')
                        ->leftJoin('magazines', 'magazines.id', '=', 'transactions.magazine_id')
                        ->leftJoin('customers', 'customers.id', '=', 'transactions.customer_id')
                        ->where($where)
                        ->get();

        return $data;
    }
}
