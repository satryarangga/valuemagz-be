<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailQueue extends Model
{
    /**
     * @var string
     */
    protected $table = 'email_queue';

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'type', 'json_object', 'processed'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';
}
