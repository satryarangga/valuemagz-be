<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Podcast extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = 'podcasts';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'status', 'created_by', 'updated_by', 'embed_code', 'banner'
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
