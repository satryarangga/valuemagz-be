<?php

namespace App\Exports;

use App\Models\Transaction;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderExport implements FromView,ShouldAutoSize
{
    public function __construct($startDate, $endDate)
    {
        $this->start = $startDate;
        $this->end = $endDate;
    }

    public function view(): View
    {
        $where = [];
        $startDate = $this->start;
        $endDate = $this->end;
        if(!empty($startDate)) {
            $where[] = ['transactions.created_at', '>', $startDate." 00:00:00"];
            $where[] = ['transactions.created_at', '<', $endDate." 23:59:59"];
        }

        $data = Transaction::select('transactions.*', 'customers.email', 'customers.phone_number', 'magazines.title as magazine_name')
                        ->leftJoin('customers', 'customers.id', '=', 'transactions.customer_id')
                        ->leftJoin('magazines', 'magazines.id', '=', 'transactions.magazine_id')
                        ->where($where)
                        ->orderBy('transactions.id', 'desc')
                        ->get();

        return view('master.transaction.excel', [
            'order' => $data
        ]);
    }
}
