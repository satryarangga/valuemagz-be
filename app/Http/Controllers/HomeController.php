<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mpdf\Mpdf;
use App\Models\Magazine;
use App\Models\Customer;
use App\Models\Transaction;
use App\Models\BundlingTransaction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function download($transactionId, $purchaseCode, $type) {
        if($type == 'paper' || $type == 'paper-en') {
            $mpdf = new Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
        } else {
            $mpdf = new Mpdf(['mode' => 'utf-8', 'format' => [148, 210]]);
        }
        $transaction = Transaction::findOrFail($transactionId);

        if($transaction->purchase_code != $purchaseCode) {
            abort(403, 'Invalid Purchase Code');
        }

        if($transaction->payment_status != 1) {
            abort(403, 'This order is not paid');
        }
        
        $magazine = Magazine::find($transaction->magazine_id);
        $customer = Customer::find($transaction->customer_id);
        $password = (!empty($customer->birthdate)) ? date('dmY', strtotime($customer->birthdate)) : 'value2021';

        if($type == 'paper') {
            $path = public_path()."/magazines/".$magazine->filename;
        } else if($type == 'paper-en') {
            $path = public_path()."/magazines-en/".$magazine->filename_en;
        } else if($type == 'digital') {
            $path = public_path()."/magazines-mobile/".$magazine->filename_mobile;
        } else if($type == 'digital-en') {
            $path = public_path()."/magazines-en-mobile/".$magazine->filename_en_mobile;
        }
        $name = "ValueMagz-".$magazine->title."-".$type.".pdf";
        
        $newPath = public_path()."/magazines-download/".$name;
        $headers = array(
            'Content-Type: application/pdf',
        );
        $filepdf = fopen($path,"r");
        if($filepdf) {
            $line_first = fgets($filepdf);
            fclose($filepdf);
        }
        else{
            echo "error opening the file.";
            die;
        }

        preg_match_all('!\d+!', $line_first, $matches);
                    
        $pdfversion = implode('.', $matches[0]);

        $floatVersion = floatval($pdfversion);

        // echo $floatVersion; die;

        if($floatVersion > 1.4) {
            // shell_exec('gs -dBATCH -dNOPAUSE -dCompatibilityLevel=1.4 -q -sDEVICE=pdfwrite -sOutputFile="'.$newPath.'" "'.$path.'"'); 
            $pageCount = $mpdf->setSourceFile($newPath);
            // $pageCount = $mpdf->setSourceFile($path);
        } else {
            $pageCount = $mpdf->setSourceFile($path);
        }

        for ($i=1; $i<=$pageCount; $i++) {
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);
    
            if ($i < $pageCount)
                $mpdf->AddPage();
        }
        
        // $tplId = $mpdf->importPage($pageCount);
        // $mpdf->UseTemplate($tplId);
        $mpdf->SetProtection(array(), 'valuemagzforlife', $password);
        $mpdf->Output($name, 'I');
    }

    public function downloadSubscriber($bundlingTransactionId, $type) {
        if($type == 'paper' || $type == 'paper-en') {
            $mpdf = new Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
        } else {
            $mpdf = new Mpdf(['mode' => 'utf-8', 'format' => [148, 210]]);
        }
        $transaction = BundlingTransaction::findOrFail($bundlingTransactionId);

        $magazine = Magazine::find($transaction->magazine_id);
        $customer = Customer::find($transaction->customer_id);
        $password = (!empty($customer->birthdate)) ? date('dmY', strtotime($customer->birthdate)) : 'value2021';

        if($type == 'paper') {
            $path = public_path()."/magazines/".$magazine->filename;
        } else if($type == 'paper-en') {
            $path = public_path()."/magazines-en/".$magazine->filename_en;
        } else if($type == 'digital') {
            $path = public_path()."/magazines-mobile/".$magazine->filename_mobile;
        } else if($type == 'digital-en') {
            $path = public_path()."/magazines-en-mobile/".$magazine->filename_en_mobile;
        }
        $name = "ValueMagz-".$magazine->title."-".$type.".pdf";
        
        $newPath = public_path()."/magazines-download/".$name;
        $headers = array(
            'Content-Type: application/pdf',
        );
        $filepdf = fopen($path,"r");
        if($filepdf) {
            $line_first = fgets($filepdf);
            fclose($filepdf);
        }
        else{
            echo "error opening the file.";
        }

        preg_match_all('!\d+!', $line_first, $matches);
                    
        $pdfversion = implode('.', $matches[0]);

        $floatVersion = floatval($pdfversion);

        if($floatVersion > 1.4) {
            // shell_exec('gs -dBATCH -dNOPAUSE -dCompatibilityLevel=1.4 -q -sDEVICE=pdfwrite -sOutputFile="'.$newPath.'" "'.$path.'"'); 
            // die('gs -dBATCH -dNOPAUSE -dCompatibilityLevel=1.4 -q -sDEVICE=pdfwrite -sOutputFile="'.$newPath.'" "'.$path.'"');
            $pageCount = $mpdf->setSourceFile($newPath);
        } else {
            $pageCount = $mpdf->setSourceFile($path);
        }

        for ($i=1; $i<=$pageCount; $i++) {
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);
    
            if ($i < $pageCount)
                $mpdf->AddPage();
        }
        
        // $tplId = $mpdf->ImportPage($pageCount);
        // $mpdf->UseTemplate($tplId);
        $mpdf->SetProtection(array(), 'valuemagzforlife', $password);
        $mpdf->Output($name, 'I');
    }
}
