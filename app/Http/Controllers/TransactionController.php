<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use App\Models\Customer;
use App\Models\BundlingMaster;
use App\Models\Magazine;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendNotifLink;
use App\Exports\OrderExport;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController extends Controller
{

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;

    public function __construct() {
        $this->module = 'master';
        $this->page = 'transaction';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');
        $data = [
            'result' => Transaction::list($startDate, $endDate),
            'page' => $this->page,
            'module' => $this->module,
            'startDate' => $startDate,
            'endDate' => $endDate
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($id, $status) {
        $data = Transaction::find($id);
        $customer = Customer::find($data->customer_id);
        $magazine = Magazine::find($data->magazine_id);

        $data->payment_status = 1;

        if($data->bundling_id == 1) {
            $bundlingMaster = BundlingMaster::find(1);
            $customer = Customer::find($data->customer_id);
            $oldQuota = $customer->bundling_quota;
            $customer->bundling_quota = $oldQuota + $bundlingMaster->quota - 1;
            $customer->save();
        }

        $data->save();

        Mail::to($customer->email)->send(new SendNotifLink($customer, $magazine, $data));

        $message = setDisplayMessage('success', "Success to change payment status and send link pdf to customer");
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendLink(Request $request, $id) {
        $data = Transaction::find($id);
        $customer = Customer::find($data->customer_id);
        $magazine = Magazine::find($data->magazine_id);

        Mail::to($customer->email)->send(new SendNotifLink($customer, $magazine, $data));

        $message = setDisplayMessage('success', "Success to send link pdf to customer");
        if($request->input('customer') == '1') {
            return redirect(route('customer.edit', ['id' => $customer->id]))->with('displayMessage', $message);
        }
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    public function export(Request $request) {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');
        $fileName = "transaction-$startDate-$endDate.xlsx";
        return Excel::download(new OrderExport($startDate, $endDate), $fileName);
    }
}
