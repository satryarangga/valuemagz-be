<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Webinar;
use App\Models\WebinarRegister;

class WebinarController extends Controller
{

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;

    public function __construct() {
        $this->module = 'master';
        $this->page = 'webinar';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Webinar::all();
        foreach($result as $key => $val) {
            $val->total_register = WebinarRegister::where('webinar_id', $val->id)->count();
        }

        $data = [
            'result' => $result,
            'page' => $this->page,
            'module' => $this->module
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localAsset = ['wysi', 'datepicker'];
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'localAsset'    => $localAsset
        ];

        return view($this->module . '/' . $this->page . ".create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'     => 'required'
        ]);

        $create = [
            'title'  => $request->input('title'),
            'description'  => $request->input('description'),
            'event_time'   => $request->input('event_time'),
            'created_by' => Auth::id(),
            'status' => 1
        ];

        if ($request->file('banner')) {
            $name = str_replace(' ', '_', $request->banner->getClientOriginalName());
            $request->banner->move(
                base_path() . '/public/images/webinar/', $name
            );
            $create['banner'] = $name;
        }

        $genre = Webinar::create($create);

        $message = setDisplayMessage('success', "Success to create new ".$this->page);
        return redirect(route($this->page . '.index'))->with('displayMessage', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $localAsset = ['wysi', 'datepicker'];
        $register = WebinarRegister::select('webinar_register.created_at', 'customers.email', 'customers.name')
                                    ->where('webinar_id', $id)
                                    ->leftJoin('customers', 'customers.id', '=', 'webinar_register.customer_id')
                                    ->get();
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => Webinar::find($id),
            'localAsset'    => $localAsset,
            'register'    => $register
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'     => 'required'
        ]);

        $data = Webinar::find($id);

        $update = [
            'title'  => $request->input('title'),
            'description'  => $request->input('description'),
            'event_time'   => $request->input('event_time'),
            'updated_by' => Auth::id(),
            'status' => 1
        ];

        if ($request->file('banner')) {
            $name = str_replace(' ', '_', $request->banner->getClientOriginalName());
            $request->banner->move(
                base_path() . '/public/images/webinar/', $name
            );
            $update['banner'] = $name;
        }

        $data->update($update);

        $message = setDisplayMessage('success', "Success to update ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Webinar::find($id);
        $message = setDisplayMessage('success', "Success to delete ".$this->page);
        $data->delete();
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($id, $status) {
        $data = Webinar::find($id);

        $data->status = $status;

        $desc = ($status == 1) ? 'activate' : 'deactivate';

        $data->save();

        $message = setDisplayMessage('success', "Success to $desc ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }
}
