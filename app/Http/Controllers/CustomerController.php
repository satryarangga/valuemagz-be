<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\Magazine;
use App\Imports\UsersImport;
use App\Models\BundlingTransaction;
use App\Models\Transaction;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;


    public function __construct() {
        $this->module = 'master';
        $this->page = 'customer';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $data = [
            'result' => Customer::all(),
            'page' => $this->page,
            'module' => $this->module
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'page' => $this->page,
            'module' => $this->module
        ];

        return view($this->module . '/' . $this->page . ".create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'     => 'required',
            'email'     => 'required|unique:users',
            'password' => 'required|string|min:4'
        ]);

        $create = [
            'name'  => $request->input('name'),
            'email'  => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ];

        $user = Customer::create($create);

        $message = setDisplayMessage('success', "Success to create new ".$this->page);
        return redirect(route($this->page . '.index'))->with('displayMessage', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $type = $request->input('type');
        $magazine = Magazine::all();
        foreach($magazine as $key => $val) {
            $where = [
                ['customer_id', '=', $id],
                ['magazine_id', '=', $val->id]
            ];
            $whereTransaction = [
                ['customer_id', '=', $id],
                ['magazine_id', '=', $val->id],
                ['payment_status', '=', 1]
            ];
            $transaction = BundlingTransaction::where($where)->get();
            $transactionSingle = Transaction::where($whereTransaction)->count();
            $val->is_owned = count($transaction) > 0 || $transactionSingle > 0;
            if($val->is_owned) {
                if($transactionSingle > 0)  {
                    $transactionSingleData = Transaction::where($whereTransaction)->first();
                    $val->transaction_id = $transactionSingleData->id;
                } else {
                    $transactionData = BundlingTransaction::where($where)->first();
                    $val->transaction_bundling_id = $transactionData->id;
                }
            }
        }
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => Customer::find($id),
            'type' => $type,
            'magazine' => $magazine
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'     => 'required',
        ]);

        $data = Customer::find($id);

        $update = [
            'name'  => $request->input('name')
        ];

        if($request->input('password')) {
            $update['password'] = bcrypt($request->input('password'));
        }

        if($request->input('birthdate')) {
            if(stristr($request->input('birthdate'), '-')) {
                $update['birthdate'] = $request->input('birthdate');
            }
        }

        $data->update($update);

        $type = $request->input('type');

        $message = setDisplayMessage('success', "Success to update ".$this->page);

        if($type == 'profile') {
            $message = setDisplayMessage('success', "Success to update your profile");
            return redirect(route($this->page.'.edit', ['id' => $id]).'?type=profile')->with('displayMessage', $message);
        }

        return redirect(route($this->page.'.edit', ['id' => $id]))->with('displayMessage', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Customer::find($id);
        $message = setDisplayMessage('success', "Success to delete ".$this->page);
        $data->delete();
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($id, $status) {
        $data = Customer::find($id);

        $data->status = $status;

        $desc = ($status == 1) ? 'activate' : 'deactivate';

        $data->save();

        $message = setDisplayMessage('success', "Success to $desc ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    public function registerTransaction() {
        $data = [
            'page' => $this->page,
            'module' => $this->module
        ];

        return view($this->module . '/' . $this->page . ".register-transaction", $data);
    }

    public function processRegisterTransaction(Request $request) {
        Excel::import(new UsersImport, $request->file('customer'));
        $message = setDisplayMessage('success', "Success to process");
        return redirect(route($this->page.'.register-transaction'))->with('displayMessage', $message);
    }
}