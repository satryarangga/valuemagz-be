<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Magazine;
use App\Models\Customer;
use App\Models\BundlingTransaction;
use App\Models\Transaction;
use App\Mail\SendNotifLinkSubscriber;
use Illuminate\Support\Facades\Mail;
use Amqp;

class MagazineController extends Controller
{

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;

    public function __construct() {
        $this->module = 'master';
        $this->page = 'magazine';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'result' => Magazine::all(),
            'page' => $this->page,
            'module' => $this->module
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localAsset = ['wysi'];
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'localAsset'    => $localAsset
        ];

        return view($this->module . '/' . $this->page . ".create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'price'     => 'required'
        ]);

        $create = [
            'title'  => $request->input('title'),
            'synopsis'  => $request->input('synopsis'),
            'price'  => $request->input('price'),
            'created_by' => Auth::id(),
            'status' => 1,
            'filename' => '',
            'filename_mobile' => ''
        ];

        if ($request->file('magazine')) {
            $name = str_replace(' ', '_', $request->magazine->getClientOriginalName());
            $request->magazine->move(
                base_path() . '/public/magazines/', $name
            );
            $create['filename'] = $name;
        }

        if ($request->file('magazine_mobile')) {
            $name = str_replace(' ', '_', $request->magazine_mobile->getClientOriginalName());
            $request->magazine_mobile->move(
                base_path() . '/public/magazines-mobile/', $name
            );
            $create['filename_mobile'] = $name;
        }

        if ($request->file('magazine_en')) {
            $name = str_replace(' ', '_', $request->magazine_en->getClientOriginalName());
            $request->magazine_en->move(
                base_path() . '/public/magazines-en/', $name
            );
            $create['filename_en'] = $name;
        }

        if ($request->file('magazine_en_mobile')) {
            $name = str_replace(' ', '_', $request->magazine_en_mobile->getClientOriginalName());
            $request->magazine_en_mobile->move(
                base_path() . '/public/magazines-en-mobile/', $name
            );
            $create['filename_en_mobile'] = $name;
        }

        if ($request->file('banner')) {
            $name = str_replace(' ', '_', $request->banner->getClientOriginalName());
            $request->banner->move(
                base_path() . '/public/images/magazines/', $name
            );
            $create['banner'] = $name;
        }
        
        if($create['filename'] == '' || $create['filename_mobile'] == '') {
            $create['buyable'] = 0;
        }

        $genre = Magazine::create($create);

        $message = setDisplayMessage('success', "Success to create new ".$this->page);
        return redirect(route($this->page . '.index'))->with('displayMessage', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $localAsset = ['wysi'];
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => Magazine::find($id),
            'localAsset'    => $localAsset
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'price'     => 'required'
        ]);

        $data = Magazine::find($id);

        $update = [
            'title'  => $request->input('title'),
            'synopsis'  => $request->input('synopsis'),
            'price'  => $request->input('price'),
            'updated_by' => Auth::id()
        ];

        if ($request->file('magazine')) {
            $name = str_replace(' ', '_', $request->magazine->getClientOriginalName());
            $request->magazine->move(
                base_path() . '/public/magazines/', $name
            );
            $update['filename'] = $name;
        }

        if ($request->file('magazine_mobile')) {
            $name = str_replace(' ', '_', $request->magazine_mobile->getClientOriginalName());
            $request->magazine_mobile->move(
                base_path() . '/public/magazines-mobile/', $name
            );
            $update['filename_mobile'] = $name;
        }

        if ($request->file('magazine_en')) {
            $name = str_replace(' ', '_', $request->magazine_en->getClientOriginalName());
            $request->magazine_en->move(
                base_path() . '/public/magazines-en/', $name
            );
            $update['filename_en'] = $name;
        }

        if ($request->file('magazine_en_mobile')) {
            $name = str_replace(' ', '_', $request->magazine_en_mobile->getClientOriginalName());
            $request->magazine_en_mobile->move(
                base_path() . '/public/magazines-en-mobile/', $name
            );
            $update['filename_en_mobile'] = $name;
        }

        if ($request->file('banner')) {
            $name = str_replace(' ', '_', $request->banner->getClientOriginalName());
            $request->banner->move(
                base_path() . '/public/images/magazines/', $name
            );
            $update['banner'] = $name;
        }

        $data->update($update);

        $message = setDisplayMessage('success', "Success to update ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Magazine::find($id);
        $message = setDisplayMessage('success', "Success to delete ".$this->page);
        $data->delete();
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($id, $status) {
        $data = Magazine::find($id);

        $data->status = $status;

        $desc = ($status == 1) ? 'activate' : 'deactivate';

        $data->save();

        $message = setDisplayMessage('success', "Success to $desc ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatusBuyable($id, $status) {
        $data = Magazine::find($id);

        $data->buyable = $status;

        $desc = ($status == 1) ? 'activate' : 'deactivate';

        $data->save();

        $message = setDisplayMessage('success', "Success to $desc selling status");
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    public function sendBundling($magazineId) {
        if(config('app.env') == 'production') {
            $customer = Customer::where('bundling_quota', '>', 0)->get();
        } else {
            $customer = Customer::where('email', 'satrya.rangga91@gmail.com')->get();
        }

        foreach($customer as $key => $val) {
            $where = [
                ['customer_id', '=', $val->id],
                ['magazine_id', '=', $magazineId],
                ['status', '=', 1]
            ];
            $whereTransaction = [
                ['customer_id', '=', $val->id],
                ['magazine_id', '=', $magazineId],
                ['payment_status', '=', 1]
            ];
            $transaction = BundlingTransaction::where($where)->get();
            $transactionSingle = Transaction::where($whereTransaction)->count();
            if(count($transaction) == 0 && $transactionSingle == 0) {
                $transaction = BundlingTransaction::create([
                    'customer_id' => $val->id,
                    'magazine_id' => $magazineId,
                    'executed_by' => Auth::id()
                ]);
                Amqp::publish('routing-key', (string) $transaction->id , ['queue' => 'valuemagz.queue', ['exchange' => 'valuemagz.exchange']]);
            }
        }

        $message = setDisplayMessage('success', "Success to send pdf to subscribers ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    public function sendBundlingExisted($bundlingId) {
        $bundlingTransaction = BundlingTransaction::find($bundlingId);
        $customer = Customer::find($bundlingTransaction->customer_id);
        $magazine = Magazine::find($bundlingTransaction->magazine_id);
        Mail::to($customer->email)->send(new SendNotifLinkSubscriber($customer, $magazine, $bundlingTransaction));
        $message = setDisplayMessage('success', "Success to send magazine");
        return redirect(route('customer.edit', ['id' => $customer->id]))->with('displayMessage', $message);
    }

    public function sendBundlingRegister($customerId, $magazineId) {
        $bundlingTransaction = BundlingTransaction::create([
            'magazine_id' => $magazineId,
            'customer_id' => $customerId,
            'executed_by' => Auth::id(),
            'status'    => 1
        ]);
        $customer = Customer::find($bundlingTransaction->customer_id);
        $magazine = Magazine::find($bundlingTransaction->magazine_id);
        Mail::to($customer->email)->send(new SendNotifLinkSubscriber($customer, $magazine, $bundlingTransaction));
        $message = setDisplayMessage('success', "Success to send magazine");
        return redirect(route('customer.edit', ['id' => $customer->id]))->with('displayMessage', $message);
    }
}
