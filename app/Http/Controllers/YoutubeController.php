<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Youtube;

class YoutubeController extends Controller
{

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;

    public function __construct() {
        $this->module = 'master';
        $this->page = 'youtube';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'result' => Youtube::all(),
            'page' => $this->page,
            'module' => $this->module
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'page' => $this->page,
            'module' => $this->module
        ];

        return view($this->module . '/' . $this->page . ".create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'     => 'required',
            'embed'     => 'required'
        ]);

        $create = [
            'title'  => $request->input('title'),
            'embed'  => $request->input('embed'),
            'created_by' => Auth::id(),
            'status' => 1
        ];

        $genre = Youtube::create($create);

        $message = setDisplayMessage('success', "Success to create new ".$this->page);
        return redirect(route($this->page . '.index'))->with('displayMessage', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => Youtube::find($id)
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'     => 'required',
            'embed'     => 'required'
        ]);

        $data = Youtube::find($id);

        $update = [
            'title'  => $request->input('title'),
            'description'  => $request->input('description'),
            'embed'  => $request->input('embed'),
            'updated_by' => Auth::id(),
            'status' => 1
        ];
        
        $data->update($update);
        $message = setDisplayMessage('success', "Success to update ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Youtube::find($id);
        $message = setDisplayMessage('success', "Success to delete ".$this->page);
        $data->delete();
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus($id, $status) {
        $data = Youtube::find($id);

        $data->status = $status;

        $desc = ($status == 1) ? 'activate' : 'deactivate';

        $data->save();

        $message = setDisplayMessage('success', "Success to $desc ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }
}
