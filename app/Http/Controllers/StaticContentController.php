<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\StaticContent;

class StaticContentController extends Controller
{

    /**
     * @var string
     */
    private $module;

    /**
     * @var string
     */
    private $page;

    public function __construct() {
        $this->module = 'master';
        $this->page = 'static-content';
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'result' => StaticContent::all(),
            'page' => $this->page,
            'module' => $this->module
        ];
        return view($this->module . '/' . $this->page . ".index", $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $localAsset = ['wysi'];
        $noEditor = ['EMBED_CODE', 'VIDEO_1', 'VIDEO_2', 'VIDEO_3', 'VIDEO_4'];
        $row = StaticContent::find($id);
        if(in_array($row->code, $noEditor)) {
            $localAsset = [];
        }
        $data = [
            'page' => $this->page,
            'module' => $this->module,
            'row' => $row,
            'localAsset'    => $localAsset
        ];

        return view($this->module . '/' . $this->page . ".edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'type'     => 'required',
            'code'     => 'required'
        ]);

        $data = StaticContent::find($id);
        
        $param = "";
        if($request->input('type') == '1') {
            $param = $request->input('content');
        } else if($request->input('type') == '2') {
            $param = $request->input('value');
        } else if($data->type == 3) {
            $name = str_replace(' ', '_', $request->image->getClientOriginalName());
            $request->image->move(
                base_path() . '/public/images/contents/', $name
            );
            $param = $name;
        }

        $update = [
            'type'  => $request->input('type'),
            'param'   => $param,
            'updated_by' => Auth::id()
        ];

        $data->update($update);

        $message = setDisplayMessage('success', "Success to update ".$this->page);
        return redirect(route($this->page.'.index'))->with('displayMessage', $message);
    }
}
