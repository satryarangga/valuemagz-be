<?php 
return [
    'bundling_email' => [
            [
                "email" => "james.wahab37@gmail.com",
                "quota" => 1
            ]
        ],
    'frontend_site' => env('FRONTEND_SITE'),
    'active_magazine' => env('ACTIVE_MAGAZINE_ID')
];